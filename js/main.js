
function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

var oldlink = readCookie('oldlink');

var sites = ['legorafi\.fr',
            'bellega\.wordpress\.com',
            'latribunedupeuple\.fr',
            'twitter\.com\/afPRESQUE',
            'bilboquet\-magazine\.fr',
            'lanouvelleobservatrice\.hautetfort\.com',
            'lecourrierdesechos\.fr',
            'baydayleaks\.tumblr\.com',
            'darons\.net','pooble\.fr',
            'sud\-ou\-est\.fr',
            'lerryberitfr\.over\-blog\.com',
            'lavoixdumorse\.fr',
            'lespratenchaine\.wordpress\.com',
            'legrosdijon\.fr',
            'twitter\.com\/sncfinfo',
            'footballfrance\.fr',
            'motodesinfo\.fr',
            'veuxjideo\.com',
            'desencyclopedie\.wikia\.com',
            'theonion\.com',
            'el\-batan\.com',
            'nordpresse\.be','axedumad\.com',
            'lepique\.com'];
var i = 0;
var query="";
while (sites[i]) {
    if (i>0) query+='|'+sites[i];
    else query+=sites[i];
    i++;
}
var thisRegex = new RegExp(query);
var link=window.location.href;

if(thisRegex.test(link)){
//chrome.browserAction.setIcon({tabId: tabId,path:"imgs/icon_alert.png"});
chrome.runtime.sendMessage({"message": "signal", "stat": true});
    if (window.location.host!=oldlink) {
//        alert(window.location.host+'!='+oldlink);
        
        alert("Attention !! Ce site peut être considéré comme :\n -Un site d'information parodique\n -Ou un site agrégateur de sites parodiques\n -Ou un compte Twitter/Facebook Parodique");
        eraseCookie('oldlink');
        createCookie('oldlink',window.location.host,0);
    }
    //eraseCookie('oldlink');
    //createCookie('oldlink',window.location.host,365);
//    oldlink=window.location.host;
} else {
    chrome.runtime.sendMessage({"message": "signal", "stat": false});
}




